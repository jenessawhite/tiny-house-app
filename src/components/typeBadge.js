import React from 'react';

const TypeBadge = (props) => {
  let lowercaseBadgeName = props.badge.toLowerCase();
  return (
    <div className={ `badge ${ lowercaseBadgeName + '-badge' }` }>
      {props.badge}
    </div>
  );
};

export default TypeBadge;