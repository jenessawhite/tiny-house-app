import React, { Component } from 'react';
import StateDropdown from './stateDropdown';

class SearchBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectField: ''
    };
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  }

  handleSelectChange(e) {
    this.setState({selectField: e.target.value});
    console.log(e.target.value);
  }

  handleOnSubmit(e) {
    e.preventDefault();
    alert('A state was submitted: ' + this.state.selectField);
  }

  render() {
    return (
      <div className="col-lg-4 search-box">
        <h3>Search Regulations</h3>
        <p>Enter in your desired location to see if they have any regulations for Tiny Home zoning.</p>

        <form onSubmit={this.handleOnSubmit}>
          <label>
          {
            this.props.page === 'zoning' ? <StateDropdown onChange={this.handleSelectChange} /> : <input type="text" name="zoning-search" id="zoning-search" className="search-input" placeholder="Search By State" />
          }
          </label>
          <input type="submit" value="Submit" className="btn btn-primary" />
        </form>
      </div>
    );
  }
}

export default SearchBox;
