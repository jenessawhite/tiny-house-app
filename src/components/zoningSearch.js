import React, {Component} from 'react';
import axios from 'axios';

import api from '../utils/api';
import SearchBox from './searchBox';
import MapWithAMarker from "./map";
import ResultsList from './resultsList';
import '../styles/listings.css';

export class ZoningSearch extends Component {
  constructor(props) {
    super(props)
    this.state = {
      center: {
        lat: 33.753746,
        lng: -84.386330
      },
      loading: true,
      selectedState: 'Georgia',
      areaData: [],
      markers: [],
      true: true
    }
  }

  componentWillMount(props) {
    this.getDropdownState();
  }

  handleMarkerClick(targetMarker) {
    this.setState({
      markers: this.state.markers.map((marker) => {
        if (marker === targetMarker) {
          return {
            ...marker,
            showInfo: true,
            markerIcon: true
          }
        } else {
          return {
            ...marker,
            showInfo: false
          }
        }
      })
    })
  }

  handleMarkerClose(targetMarker) {
    this.setState({
      markers: this.state.markers.map((marker) => {
        if (marker === targetMarker) {
          return {
            ...marker,
            showInfo: false
          }
        }
        return marker
      })
    })
  }

  handleMarkerClose2(targetMarker) {
    this.setState({
      markers: this.state.markers.map((marker) => {
        if (targetMarker) {
          return {
            ...marker,
            showInfo: false
          }
        }
        return marker
      })
    })
  }

  getDropdownState() {
    axios.get(api() + 'zoning/' + this.state.selectedState, {
      headers: {'Access-Control-Allow-Origin': '*'}
    })
    .then(response => {
      console.log(response);
      this.setState({
        markers: response.data
      })
      setTimeout(() => {
        this.setState({
          loading: false          
        })
      }, 3000)
    })
    .catch(function(error) {
      console.log(error);
    });
  }

  render() {
    // render loading when app is not ready
    if(this.state.loading) {
      return (
        <div className="text-center">
          <div className="icon-spin">
            <i className="icon ion-load-c"></i>
          </div>  
        </div>
      )
    }

    return (
      <div className="row search-map-wrapper">
        <SearchBox searchType="zoning-search" page="zoning" />  
        <div className="col-lg-8">
        <MapWithAMarker
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div className="mapWrapper" />}
          mapElement={<div style={{ height: `100%` }} />}
          center={this.state.center}
          onMapClick={this.handleMarkerClose2.bind(this)}
          onMarkerClick={this.handleMarkerClick.bind(this)}
          markers={this.state.markers}
          onMarkerClose={this.handleMarkerClose.bind(this)} />  

        <div className="card results-count">
          <p>{this.state.markers.length} Results</p>
          <div className="key">
            <h5>Key</h5>    
            <div className="sa-badge badge">SA<span> - Stand Alone Units</span></div>
            <div className="thc-badge badge">THC<span> - Tiny Home Community</span></div>
            <div className="adu-badge badge">ADU<span> - Accessory Dwelling Units</span></div> 
            <div className="thow-badge badge">THOW<span> - Tiny Home on Wheels</span></div>
            </div>
        </div>
          
        { this.state.markers.map((result, i) => {
          return (
            <ResultsList key={i} result={result} />
          )
        }) }
        </div>
      </div>
    );
  }
}

export default ZoningSearch;