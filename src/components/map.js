import React from 'react';
import {
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";

import MapInfoWindow from './mapInfoWindow';

const MapWithAMarker = withGoogleMap((props) => {
  
  return (
    <GoogleMap
      ref={props.onMapLoad}
      defaultZoom={10}
      center={props.center}
      onClick={props.onMapClick}
    >
      {props.markers.map((marker, index) => {
        let lat = parseFloat(marker.latPos)
        let lng = parseFloat(marker.longPos)
        return (
          <Marker
            key={index}
            position={ {lat: lat, lng: lng} }
            onClick={() => props.onMarkerClick(marker)}
            {...marker}
          >
            {marker.showInfo && (
              <MapInfoWindow onCloseClick={() => props.onMarkerClose(marker)} {...marker} />
            )}
            </Marker>
        )
      })}
    </GoogleMap>
  )
})

export default MapWithAMarker;
