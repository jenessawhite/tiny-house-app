import React from 'react';

import TypeBadge from './typeBadge';

const ResultsList = (props) => {
  return (
    <div className="card results-card col">              
      <div className="card-body row">
        <div className="card-info-wrap">
          <h4 className="card-title">{props.result.name}</h4>
          { props.result.countyName ? (<h6 className="card-subtitle mb-2 text-muted">{ props.result.countyName } County</h6>) : null }
          <div className="badge-row">
            { props.result.unitTypes.map((badge, index) => {
              return (
                <TypeBadge key={index} badge={badge} />
              );
            }) }  
          </div>
        </div>  
        { props.result.infoLink ? (<p className="card-text">Follow link for more info: <a href={ props.result.infoLink } target="_blank">{ props.result.name }</a></p>) : null }
      </div>
    </div>
  );
};

export default ResultsList;