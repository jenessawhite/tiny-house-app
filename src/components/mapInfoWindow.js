import React from 'react';
import { InfoWindow } from "react-google-maps";

import TypeBadge from './typeBadge';

const MapInfoWindow = (props) => {
  let marker

  return (
    <InfoWindow onCloseClick={() => props.onCloseClick(marker)} id={props.id}>
      <div>
        <h6>{props.name}</h6>
        { props.countyName ? (<p className="muted-text">{props.countyName} County</p>) : null }
        <div className="badge-row">
          {
            props.unitTypes.map((badge, index) => {
              return (
                <TypeBadge key={index} badge={badge} />
              );
            })
          } 
        </div>
      </div>
    </InfoWindow>
  );
};

export default MapInfoWindow;