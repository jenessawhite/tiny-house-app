import React from 'react';
import { Marker } from "react-google-maps";
import MapInfoWindow from "./mapInfoWindow";

class MarkerContainer extends React.Component {
  state = {open: false}

  onClick = () => {
    this.setState({open: !this.state.open})
  }

  render() {
    const position = this.props.position;
    return (
      <span>
      <Marker onClick={this.onClick} position={position}></Marker>
      <InfoBox
          defaultPosition={new google.maps.LatLng(position.lat, position.lng)}
          options={{ closeBoxURL: ``, enableEventPropagation: true, visible: this.state.open }}
      >
        <div style={{ backgroundColor: `yellow`, opacity: 0.75, padding: `12px` }}>
          <div style={{ fontSize: `16px`, fontColor: `#08233B` }}>
            Hello from Taipei
          </div>
        </div>
      </InfoBox>
      </span>
    );
  }
}

export default MarkerContainer;