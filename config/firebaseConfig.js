import firebase, { auth, provider } from './firebase.js';;

// Initialize Firebase
const config = {
  apiKey: "AIzaSyA7kfzCKvJc9KieCnrhqYkKZAAAUDZFigc",
  authDomain: "hompact-test-1508694727034.firebaseapp.com",
  databaseURL: "https://hompact-test-1508694727034.firebaseio.com",
  projectId: "hompact-test-1508694727034",
  storageBucket: "hompact-test-1508694727034.appspot.com",
  messagingSenderId: "122795640298"
};
firebase.initializeApp(config);

export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();

export default firebase;