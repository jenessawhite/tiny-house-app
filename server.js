// Include Server Dependencies
require('dotenv').config();
const express = require('express');
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');
const router = express.Router();
const config = require('./config/config');

var seed;
if(process.env.NODE_ENV !== 'production') {
  seed = require('seedquelize')
}

var Sequelize = require('sequelize');

// Connect to a sql database
if (process.env.NODE_ENV == 'production') {
  var sequelize = new Sequelize(process.env.DATABASE_URL, {
    dialect: 'postgres',
    protocol: 'postgres',
  });
} else {
  var sequelize = new Sequelize(process.env.DATABASE_URL_DEV, {
    dialect: 'postgres',
    protocol: 'postgres',
  });
}

// We need to define models. A model describes the structure of
// something that we want to store in the database. On each model
// we define each of the fields on that model. We can also decide
// to have different internal and external field names. Internal
// fields would be the column names in the database, external are
// what you would use in your code.

//Define Area which will include our counties/cities and their information
//We may need to add to this in the future to show more types of units
const Area = sequelize.define(
  'area',
  {
    name: {
      type: Sequelize.STRING,
      field: 'name',
    },
    type: {
      type: Sequelize.STRING,
      field: 'type',
    },
    countyName: {
      type: Sequelize.STRING,
      field: 'countyName',
    },
    stateName: {
      type: Sequelize.STRING,
      field: 'stateName',
    },
    latPos: {
      type: Sequelize.DECIMAL,
      field: 'latPos',
    },
    longPos: {
      type: Sequelize.DECIMAL,
      field: 'longPos',
    },
    supportsTH: {
      type: Sequelize.BOOLEAN,
      field: 'supportsTH',
    },
    description: {
      type: Sequelize.TEXT,
      field: 'description',
    },
    infoLink: {
      type: Sequelize.STRING,
      field: 'infoLink',
    },
    unitTypes: {
      type: Sequelize.ARRAY(Sequelize.STRING),
      field: 'unitTypes',
    },
  },
  {
    freezeTableName: true,
  }
);

// define the homes model that we'll use to display our home listings
const Homes = sequelize.define(
  'homes',
  {
    title: {
      type: Sequelize.STRING,
      field: 'title',
    },
    description: {
      type: Sequelize.TEXT,
      field: 'description',
    },
    location: {
      type: Sequelize.STRING,
      field: 'location',
    },
    type: {
      type: Sequelize.STRING,
      field: 'type',
    },
    photo1: {
      type: Sequelize.STRING,
      field: 'photo',
    },
    photo2: {
      type: Sequelize.STRING,
      field: 'photo2',
    },
    photo3: {
      type: Sequelize.STRING,
      field: 'photo3',
    },
    photo4: {
      type: Sequelize.STRING,
      field: 'photo4',
    },
    photo5: {
      type: Sequelize.STRING,
      field: 'photo5',
    },
    saleOrRent: {
      type: Sequelize.STRING,
      field: 'saleOrRent',
    },
    bedrooms: {
      type: Sequelize.INTEGER,
      field: 'bedrooms',
    },
    bathrooms: {
      type: Sequelize.INTEGER,
      field: 'bathrooms',
    },
    hasLand: {
      type: Sequelize.BOOLEAN,
      field: 'hasLand',
    },
    acres: {
      type: Sequelize.INTEGER,
      field: 'acres',
    },
    yearBuilt: {
      type: Sequelize.STRING,
      field: 'yearBuilt',
    },
    features: {
      type: Sequelize.STRING,
      field: 'features',
    },
    price: {
      type: Sequelize.INTEGER,
      field: 'price',
    },
    size: {
      type: Sequelize.INTEGER,
      field: 'size',
    },
  },
  {
    freezeTableName: true,
  }
);

//Define State
const State = sequelize.define(
  'state',
  {
    name: {
      type: Sequelize.STRING,
      field: 'name',
    },
    alpha2: {
      type: Sequelize.STRING,
      field: 'alpha2',
    },
  },
  {
    freezeTableName: true,
  }
);

//Associate Areas and Homes to State
Area.belongsTo(State);
Homes.belongsTo(State);
State.hasMany(Area);
State.hasMany(Homes);

// THIS IS THE DON'T GET FIRED CLAUSE
// Seeding (or preloading) your database gives it dummy data
// so that development isn't a graveyard. In production we
// probably don't want to delete the entire database :)
if (process.env.NODE_ENV !== 'production') {
  sequelize.sync({force: true}).then(() => {
    // Some sample states
    const states = {
      data: [
        {
          name: 'California',
          alpha2: 'CA'
        },
        {
          name: 'Georgia',
          alpha2: 'GA'
        },
        ,
      ],
      model: State,
    };
    // Some sample areas
    const areas = {
      data: [
        {
          name: 'Chamblee',
          type: 'city',
          countyName: 'DeKalb',
          stateName: 'Georgia',
          latPos: '33.8920',
          longPos: '-84.2988',
          supportsTH: true,
          description: '',
          infoLink: 'http://tinyhouseatlanta.com/wp-content/uploads/2017/04/Chamblee-.pdf',
          unitTypes: [ 'ADU', 'SA', 'THC', 'THOW' ]
        },
        {
          name: 'East Point',
          type: 'city',
          countyName: 'Fulton',
          stateName: 'Georgia',
          latPos: '33.6796',
          longPos: '-84.4394',
          supportsTH: true,
          description: '',
          infoLink: 'http://tinyhouseatlanta.com/wp-content/uploads/2017/04/East-Point-6-01-17.pdf',
          unitTypes: [ 'ADU', 'SA' ]
        },
        {
          name: 'Atlanta',
          type: 'city',
          countyName: 'Fulton',
          stateName: 'Georgia',
          latPos: '33.753746',
          longPos: '-84.386330',
          supportsTH: true,
          description: '',
          infoLink: 'http://tinyhouseatlanta.com/wp-content/uploads/2017/04/Atlanta-4-29-17.pdf',
          unitTypes: [ 'ADU', 'SA', 'THOW' ]
        },
        {
          name: 'Decatur',
          type: 'city',
          countyName: 'DeKalb',
          stateName: 'Georgia',
          latPos: '33.7748',
          longPos: '-84.2963',
          supportsTH: true,
          description: '',
          infoLink: 'http://tinyhouseatlanta.com/wp-content/uploads/2017/04/Decatur-summary-4-27-17.pdf',
          unitTypes: [ 'ADU', 'SA', 'THC' ]
        },
      ],
      model: Area,
    };

    seed([
      states,
      areas,
    ]).then(() => {
      startExpress();
    });
  });
} else {
  startExpress();
}

// Start Instance of Express
function startExpress() {

  // Create a new express app to serve our api
  const app = express();

  // Teach express how to parse requests of type application/json
  app.use(bodyParser.json());

  // Teach express how to parse requests of type application/x-www-form-urlencoded
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.text());

  
  // Enable CORS so that browsers don't block requests.
  app.use(cors());
  // BUILD ROUTES USING SEQUELIZE HERE
  
  // --------------STATES-------------------------
  
  // Get all states
  app.get('/api/states', (req, res) => {
    // Find all states
    State.findAll().then((states) => {
      console.log(states);
      res.json(states);
    }).catch(err => {
      console.log(err);
    })
  });
  
  // --------------AREAS-------------------------
  
  // Get all areas for zoning
  app.get('/api/zoning/:state', (req, res) => {
    // Find all areas
    Area.findAll({
      where: {
        stateName: req.params.state
      }
    }).then((area) => {
      console.log(area);
      res.json(area);
    }).catch(err => {
      console.log(err);
      return res.json({
        msg: 'No information is available for the state requested.',
      });
    })
  });
  // //get state info from zoning route
  // app.get('/zoning/:state', function (req, res) {
  //   var state = req.params.state;
  //   //get state info for California
  //   if (state === 'California') {
  //     var californiaQuery = 'select * from CaliforniaTinyHouses';
  //     connection.query(californiaQuery, (error1, results1, fields1) => {
  //       if (error1) throw error;
  //       return res.json(results1);
  //     });
  //   } else if (state === 'Georgia') {
  //     //get state info for Georgia
  //     var georgiaQuery = 'select * from GeorgiaTinyHouses';
  //     connection.query(georgiaQuery, (error2, results2, fields2) => {
  //       if (error2) throw error;
  //       return res.json(results2);
  //     });
  //   } else {
  //     //if state is neither GA nor CA, return an error
  //     return res.json({
  //       msg: 'No information is available for the state requested.',
  //     });
  //   }
  // });
                
// -------------------------------------------------
  const PORT = process.env.PORT || 4000; // Sets an initial port. We’ll use this later in our listener

  app.use(express.static(path.join(__dirname, 'build')));
  
  app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, 'build', '200.html'));
  });

  // Actually start the server
  app.listen(PORT, () => {
    console.log('App listening on PORT: ' + PORT);
  });
}