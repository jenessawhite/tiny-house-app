# Hompact App

This web application is built by Tiny Home lovers, for Tiny Home lovers.
See the designs here: [Invision App](https://invis.io/XDD3DF0H7)

### Prerequisites

You'll need NodeJs to run this app.

### Installing

1. Make sure you have NodeJS installed
2. Clone the repo
3. Install the dependencies, by running `yarn` in your terminal
4. You'll need to copy the `.env.example` file to a `.env` and enter in the proper information
5. Once dependencies are installed, run `yarn start` to start React App
6. Open a new terminal window and run `node server.js`

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [ReactJs](https://facebook.github.io/react/docs/)
* [ExpressJs](https://expressjs.com/)
* [MySql](https://www.mysql.com/)



## Acknowledgments

* [Women Who Code Atl](https://www.womenwhocode.com/atlanta)
